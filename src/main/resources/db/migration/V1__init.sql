DROP TABLE IF EXISTS user CASCADE;
CREATE TABLE user (
  id                 UUID PRIMARY KEY NOT NULL,
  app_carousel_id    UUID,
  date_created       TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP,
  last_updated       TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP,
  first_name         VARCHAR,
  last_name          VARCHAR,
  email              VARCHAR(128),
  language           VARCHAR(4),
  open_id            VARCHAR,
  company_department VARCHAR,
  company_title      VARCHAR,
  hourly_rate        VARCHAR(128),
  zip_code           VARCHAR(16),
  time_zone          VARCHAR,
  favorite_color     VARCHAR,
  app_admin          BOOLEAN          NOT NULL DEFAULT FALSE
);

DROP TABLE IF EXISTS event CASCADE;
CREATE TABLE event (
  id              UUID PRIMARY KEY NOT NULL,
  date_created    TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP,
  last_updated    TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP,
  type            VARCHAR,
  request_url     VARCHAR,
  return_url      VARCHAR,
  flag            VARCHAR(128),
  transaction_log VARCHAR,
  error_log       VARCHAR,
  creator_id      UUID,
);

DROP TABLE IF EXISTS company CASCADE;
CREATE TABLE company (
  id              UUID PRIMARY KEY NOT NULL,
  app_carousel_id UUID,
  date_created    TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP,
  last_updated    TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP,
  name            VARCHAR,
  email           VARCHAR,
  phone_number    VARCHAR,
  website         VARCHAR,
  country         VARCHAR
);

DROP TABLE IF EXISTS market_place CASCADE;
CREATE TABLE market_place (
  id           UUID PRIMARY KEY NOT NULL,
  date_created TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP,
  last_updated TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP,
  partner      VARCHAR,
  base_url     VARCHAR,
);

DROP TABLE IF EXISTS subscription CASCADE;
CREATE TABLE subscription (
  id              UUID PRIMARY KEY NOT NULL,
  date_created    TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP,
  last_updated    TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP,
  edition_code    VARCHAR,
  status          VARCHAR,
  market_place_id UUID,
  user_id         UUID
);