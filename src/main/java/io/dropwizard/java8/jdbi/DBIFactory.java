package io.dropwizard.java8.jdbi;

import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.db.ManagedDataSource;
import io.dropwizard.java8.jdbi.args.*;
import io.dropwizard.setup.Environment;
import org.flywaydb.core.Flyway;
import org.skife.jdbi.v2.DBI;

public class DBIFactory extends io.dropwizard.jdbi.DBIFactory {

  public DBI build(Environment environment, DataSourceFactory configuration, String name) {
    final ManagedDataSource dataSource = configuration.build(environment.metrics(), name);
    return this.build(environment, configuration, dataSource, name);
  }

  public DBI build(Environment environment, DataSourceFactory configuration, ManagedDataSource dataSource, String name) {
    final DBI dbi = super.build(environment, configuration, dataSource, name);

    if (configuration.getUrl().startsWith("jdbc:h2:")) {
      Flyway flyway = new Flyway();
      flyway.setDataSource(dataSource);
      flyway.clean();
      flyway.migrate();
    }

    dbi.registerArgumentFactory(new OptionalArgumentFactory(configuration.getDriverClass()));
    dbi.registerContainerFactory(new OptionalContainerFactory());
    dbi.registerArgumentFactory(new LocalDateArgumentFactory());
    dbi.registerArgumentFactory(new LocalDateTimeArgumentFactory());
    dbi.registerArgumentFactory(new InstantArgumentFactory());
    dbi.registerMapper(new InstantMapper());
    dbi.registerMapper(new LocalDateMapper());
    dbi.registerMapper(new LocalDateTimeMapper());

    return dbi;
  }
}
