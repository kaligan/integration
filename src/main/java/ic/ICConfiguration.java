package ic;

import com.fasterxml.jackson.annotation.JsonProperty;
import ic.db.HikariDataSourceFactory;
import io.dropwizard.Configuration;
import io.dropwizard.client.JerseyClientConfiguration;
import io.dropwizard.flyway.FlywayFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


/**
 * Created by: mnicholson
 * Date: 29/09/15.
 */
public class ICConfiguration extends Configuration {

  @Valid
  @NotNull
  @JsonProperty("hikari")
  private HikariDataSourceFactory hikari = new HikariDataSourceFactory();

  public HikariDataSourceFactory getHikariDataSourceFactory() {
    return hikari;
  }

  @Valid
  @NotNull
  @JsonProperty("flyway")
  private FlywayFactory flyway = new FlywayFactory();

  public FlywayFactory getFlywayFactory() {
    return flyway;
  }

  @Valid
  @NotNull
  @JsonProperty("cacheViews")
  private Boolean cacheViews = true;

  public Boolean getCacheViews() {
    return cacheViews;
  }

  @Valid
  @NotNull
  @JsonProperty("jerseyClient")
  private JerseyClientConfiguration httpClient = new JerseyClientConfiguration();

  public JerseyClientConfiguration getJerseyClientConfiguration() {
    return httpClient;
  }
}
