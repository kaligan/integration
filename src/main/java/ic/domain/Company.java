package ic.domain;

import javax.xml.bind.annotation.XmlRootElement;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

/**
 * Created by mnicholson on 16/10/15.
 */
@XmlRootElement()
public class Company {
  private UUID id;
  private UUID appCarouselId;
  private Instant dateCreated;
  private Instant lastUpdated;
  private String name;
  private String email;
  private String phoneNumber;
  private String website;
  private String country;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getAppCarouselId() {
    return appCarouselId;
  }

  public void setAppCarouselId(UUID appCarouselId) {
    this.appCarouselId = appCarouselId;
  }

  public Instant getDateCreated() {
    return dateCreated;
  }

  public void setDateCreated(Instant dateCreated) {
    this.dateCreated = dateCreated;
  }

  public Instant getLastUpdated() {
    return lastUpdated;
  }

  public void setLastUpdated(Instant lastUpdated) {
    this.lastUpdated = lastUpdated;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getWebsite() {
    return website;
  }

  public void setWebsite(String website) {
    this.website = website;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Company company = (Company) o;
    return Objects.equals(id, company.id) &&
        Objects.equals(appCarouselId, company.appCarouselId) &&
        Objects.equals(dateCreated, company.dateCreated) &&
        Objects.equals(lastUpdated, company.lastUpdated) &&
        Objects.equals(name, company.name) &&
        Objects.equals(email, company.email) &&
        Objects.equals(phoneNumber, company.phoneNumber) &&
        Objects.equals(website, company.website) &&
        Objects.equals(country, company.country);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, appCarouselId, dateCreated, lastUpdated, name, email, phoneNumber, website, country);
  }

  @Override
  public String toString() {
    return "Company{" +
        "id=" + id +
        ", appCarouselId=" + appCarouselId +
        ", dateCreated=" + dateCreated +
        ", lastUpdated=" + lastUpdated +
        ", name='" + name + '\'' +
        ", email='" + email + '\'' +
        ", phoneNumber='" + phoneNumber + '\'' +
        ", website='" + website + '\'' +
        ", country='" + country + '\'' +
        '}';
  }
}
