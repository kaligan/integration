package ic.domain;

import ic.resources.dto.UserDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.annotation.XmlRootElement;
import java.time.Instant;
import java.util.Map;
import java.util.UUID;

/**
 * Author: mnicholson
 * Date: 2015/10/18
 */
@XmlRootElement()
public class User {
  private final Logger logger = LoggerFactory.getLogger(User.class);
  private UUID id;
  private UUID appCarouselId;
  private Instant dateCreated;
  private Instant lastUpdated;

  private UUID companyId;
  private Company company;

  private String openId;
  private String email;
  private String firstName;
  private String lastName;
  private String address;
  private String language;

  private String companyDepartment;
  private String companyTitle;
  private String hourlyRate;
  private String zipCode;
  private String timeZone;
  private String favoriteColor;

  private boolean active;
  private boolean appAdmin;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getAppCarouselId() {
    return appCarouselId;
  }

  public void setAppCarouselId(UUID appCarouselId) {
    this.appCarouselId = appCarouselId;
  }

  public Instant getDateCreated() {
    return dateCreated;
  }

  public void setDateCreated(Instant dateCreated) {
    this.dateCreated = dateCreated;
  }

  public Instant getLastUpdated() {
    return lastUpdated;
  }

  public void setLastUpdated(Instant lastUpdated) {
    this.lastUpdated = lastUpdated;
  }

  public String getOpenId() {
    return openId;
  }

  public void setOpenId(String openId) {
    this.openId = openId;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public String getCompanyDepartment() {
    return companyDepartment;
  }

  public void setCompanyDepartment(String companyDepartment) {
    this.companyDepartment = companyDepartment;
  }

  public String getCompanyTitle() {
    return companyTitle;
  }

  public void setCompanyTitle(String companyTitle) {
    this.companyTitle = companyTitle;
  }

  public String getHourlyRate() {
    return hourlyRate;
  }

  public void setHourlyRate(String hourlyRate) {
    this.hourlyRate = hourlyRate;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public String getTimeZone() {
    return timeZone;
  }

  public void setTimeZone(String timeZone) {
    this.timeZone = timeZone;
  }

  public String getFavoriteColor() {
    return favoriteColor;
  }

  public void setFavoriteColor(String favoriteColor) {
    this.favoriteColor = favoriteColor;
  }

  public boolean isAppAdmin() {
    return appAdmin;
  }

  public void setAppAdmin(boolean appAdmin) {
    this.appAdmin = appAdmin;
  }

  public void setAttributes(Map<String, String> attributes) {
    if (null != attributes) {
      attributes.entrySet().forEach(it -> {
        switch (it.getKey()) {
          case "companyDepartment":
            this.companyDepartment = it.getValue();
            break;
          case "companyTitle":
            this.companyTitle = it.getValue();
            break;
          case "hourlyRate":
            this.hourlyRate = it.getValue();
            break;
          case "zipCode":
            this.zipCode = it.getValue();
            break;
          case "timeZone":
            this.timeZone = it.getValue();
            break;
          case "favoriteColor":
            this.favoriteColor = it.getValue();
            break;
          case "appAdmin":
            this.appAdmin = Boolean.valueOf(it.getValue());
            break;
          default:
            logger.error("Unknown User attribute type: " + it.getKey());
            break;
        }
      });
    }
  }


  @Override
  public String toString() {
    return "User{" +
        "id=" + id +
        ", appCarouselId=" + appCarouselId +
        ", dateCreated=" + dateCreated +
        ", lastUpdated=" + lastUpdated +
        ", openId='" + openId + '\'' +
        ", email='" + email + '\'' +
        ", firstName='" + firstName + '\'' +
        ", lastName='" + lastName + '\'' +
        ", address='" + address + '\'' +
        ", language='" + language + '\'' +
        ", companyDepartment='" + companyDepartment + '\'' +
        ", companyTitle='" + companyTitle + '\'' +
        ", hourlyRate='" + hourlyRate + '\'' +
        ", zipCode='" + zipCode + '\'' +
        ", timeZone='" + timeZone + '\'' +
        ", favoriteColor='" + favoriteColor + '\'' +
        ", appAdmin=" + appAdmin +
        '}';
  }
}
