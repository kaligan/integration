package ic.domain;

import javax.xml.bind.annotation.XmlRootElement;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

/**
 * Created by mnicholson on 21/10/15.
 */
@XmlRootElement()
public class Subscription {
  private UUID id;
  private Instant dateCreated;
  private Instant lastUpdated;

  private String editionCode;
  private Status status = Status.FREE_TRIAL;
  private UUID marketPlaceId;
  private MarketPlace marketPlace;

  private UUID userId;
  private User user;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public Instant getDateCreated() {
    return dateCreated;
  }

  public void setDateCreated(Instant dateCreated) {
    this.dateCreated = dateCreated;
  }

  public Instant getLastUpdated() {
    return lastUpdated;
  }

  public void setLastUpdated(Instant lastUpdated) {
    this.lastUpdated = lastUpdated;
  }

  public String getEditionCode() {
    return editionCode;
  }

  public void setEditionCode(String editionCode) {
    this.editionCode = editionCode;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  public UUID getMarketPlaceId() {
    return marketPlaceId;
  }

  public void setMarketPlaceId(UUID marketPlaceId) {
    this.marketPlaceId = marketPlaceId;
  }

  public MarketPlace getMarketPlace() {
    return marketPlace;
  }

  public void setMarketPlace(MarketPlace marketPlace) {
    this.marketPlace = marketPlace;
  }

  public UUID getUserId() {
    return userId;
  }

  public void setUserId(UUID userId) {
    this.userId = userId;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public enum Status {
    FREE_TRIAL,
    FREE_TRIAL_EXPIRED,
    ACTIVE,
    SUSPENDED,
    CANCELED
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Subscription that = (Subscription) o;
    return Objects.equals(id, that.id) &&
        Objects.equals(dateCreated, that.dateCreated) &&
        Objects.equals(lastUpdated, that.lastUpdated) &&
        Objects.equals(editionCode, that.editionCode) &&
        status == that.status &&
        Objects.equals(marketPlaceId, that.marketPlaceId) &&
        Objects.equals(userId, that.userId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, dateCreated, lastUpdated, editionCode, status, marketPlaceId, userId);
  }

  @Override
  public String toString() {
    return "Subscription{" +
        "id=" + id +
        ", dateCreated=" + dateCreated +
        ", lastUpdated=" + lastUpdated +
        ", editionCode='" + editionCode + '\'' +
        ", status=" + status +
        ", marketPlaceId=" + marketPlaceId +
        ", userId=" + userId +
        '}';
  }
}
