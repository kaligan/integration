package ic.domain;

import javax.xml.bind.annotation.XmlRootElement;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

/**
 * Author: mnicholson
 * Date: 2015/10/21
 */
@XmlRootElement()
public class MarketPlace {
  private UUID id;
  private Instant dateCreated;
  private Instant lastUpdated;
  private String partner;
  private String baseUrl;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public Instant getDateCreated() {
    return dateCreated;
  }

  public void setDateCreated(Instant dateCreated) {
    this.dateCreated = dateCreated;
  }

  public Instant getLastUpdated() {
    return lastUpdated;
  }

  public void setLastUpdated(Instant lastUpdated) {
    this.lastUpdated = lastUpdated;
  }

  public String getPartner() {
    return partner;
  }

  public void setPartner(String partner) {
    this.partner = partner;
  }

  public String getBaseUrl() {
    return baseUrl;
  }

  public void setBaseUrl(String baseUrl) {
    this.baseUrl = baseUrl;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    MarketPlace that = (MarketPlace) o;
    return Objects.equals(id, that.id) &&
        Objects.equals(dateCreated, that.dateCreated) &&
        Objects.equals(lastUpdated, that.lastUpdated) &&
        Objects.equals(partner, that.partner) &&
        Objects.equals(baseUrl, that.baseUrl);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, dateCreated, lastUpdated, partner, baseUrl);
  }

  @Override
  public String toString() {
    return "MarketPlace{" +
        "id=" + id +
        ", dateCreated=" + dateCreated +
        ", lastUpdated=" + lastUpdated +
        ", partner='" + partner + '\'' +
        ", baseUrl='" + baseUrl + '\'' +
        '}';
  }
}
