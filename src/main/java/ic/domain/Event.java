package ic.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import ic.resources.dto.EventDTO;
import ic.resources.dto.MarketPlaceDTO;
import ic.resources.dto.PayloadDTO;
import ic.resources.media.json.InstantAdapter;
import ic.resources.media.json.InstantSerializer;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.Instant;
import java.util.UUID;

/**
 * Created by mnicholson on 16/10/15.
 */
@XmlRootElement()
public class Event {
  private UUID id;
  private Instant dateCreated;
  private Instant lastUpdated;

  private Type type;
  private String requestUrl;
  private String returnUrl;
  private String flag;
  private String transactionLog;
  private String errorLog;
  private UUID creatorId;
  private User creator;

  private PayloadDTO payload;
  private MarketPlaceDTO marketPlace;

  public Event(Builder builder) {
    this.id = builder.id;
    this.dateCreated = builder.dateCreated;
    this.lastUpdated = builder.lastUpdated;
    this.type = builder.type;
    this.requestUrl = builder.requestUrl;
    this.returnUrl = builder.returnUrl;
    this.flag = builder.flag;
    this.transactionLog = builder.transactionLog;
    this.errorLog = builder.errorLog;
    this.creatorId = builder.creatorId;
    this.creator = builder.creator;
    this.payload = builder.payload;
    this.marketPlace = builder.marketPlace;
  }

  public UUID getId() {
    return id;
  }

  @JsonSerialize(using = InstantSerializer.class)
  public Instant getDateCreated() {
    return dateCreated;
  }

  @JsonSerialize(using = InstantSerializer.class)
  public Instant getLastUpdated() {
    return lastUpdated;
  }

  public Type getType() {
    return type;
  }

  public String getRequestUrl() {
    return requestUrl;
  }

  public String getReturnUrl() {
    return returnUrl;
  }

  public String getFlag() {
    return flag;
  }

  public String getTransactionLog() {
    return transactionLog;
  }

  public String getErrorLog() {
    return errorLog;
  }

  public UUID getCreatorId() {
    return creatorId;
  }

  public User getCreator() {
    return creator;
  }

  public PayloadDTO getPayload() {
    return payload;
  }

  public MarketPlaceDTO getMarketPlace() {
    return marketPlace;
  }

  public enum Type {
    SUBSCRIPTION_ORDER,
    SUBSCRIPTION_CHANGE,
    SUBSCRIPTION_CANCEL,
    SUBSCRIPTION_NOTICE,
    USER_ASSIGNMENT,
    USER_UNASSIGNMENT
  }

  public static class Builder {
    private UUID id = UUID.randomUUID();
    private Instant dateCreated = Instant.now();
    private Instant lastUpdated = Instant.now();
    private Event.Type type;
    private String requestUrl;
    private String returnUrl;
    private String flag;
    private String transactionLog;
    private String errorLog;
    private UUID creatorId;
    private User creator;
    private PayloadDTO payload;
    private MarketPlaceDTO marketPlace;

    public Builder withEventDTO(EventDTO event) {
      this.type = event.getType();
      this.returnUrl = event.getReturnUrl();
      this.flag = event.getFlag();
      this.creator = event.getCreator().getUserInstance();
      this.payload = event.getPayload();
      this.marketPlace = event.getMarketplace();
      return this;
    }

    public Builder withId(UUID uuid) {
      this.id = uuid;
      return this;
    }

    public Builder withDateCreated(Instant dateCreated) {
      this.dateCreated = dateCreated;
      return this;
    }

    public Builder withLastUpdated(Instant lastUpdated) {
      this.lastUpdated = lastUpdated;
      return this;
    }

    public Builder withType(Event.Type type) {
      this.type = type;
      return this;
    }

    public Builder withRequestUrl(String requestUrl) {
      this.requestUrl = requestUrl;
      return this;
    }

    public Builder withReturnUrl(String returnUrl) {
      this.returnUrl = returnUrl;
      return this;
    }

    public Builder withFlag(String flag) {
      this.flag = flag;
      return this;
    }

    public Builder withTransactionLog(String transactionLog) {
      this.transactionLog = transactionLog;
      return this;
    }

    public Builder withErrorLog(String errorLog) {
      this.errorLog = errorLog;
      return this;
    }

    public Builder withCreatorId(UUID creatorId) {
      this.creatorId = creatorId;
      return this;
    }

    public Builder withCreator(User creator) {
      this.creator = creator;
      return this;
    }

    public Builder withPayload(PayloadDTO payload) {
      this.payload = payload;
      return this;
    }

    public Builder withMarketPlace(MarketPlaceDTO marketPlace) {
      this.marketPlace = marketPlace;
      return this;
    }

    public Event create() {
      return new Event(this);
    }
  }
}
