package ic.service;

import com.google.common.base.Strings;
import ic.db.jdbi.MarketPlaceDAO;
import ic.domain.MarketPlace;
import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.UUID;

/**
 * Created by mnicholson on 21/10/15.
 */
public class MarketService {
  private final Logger logger = LoggerFactory.getLogger(UserService.class);
  private final DBI dbi;
  private final MarketPlaceDAO marketPlaceDAO;

  public MarketService(DBI dbi) {
    this.dbi = dbi;
    this.marketPlaceDAO = dbi.onDemand(MarketPlaceDAO.class);
  }


  public void save(MarketPlace marketPlace) {
    marketPlace.setLastUpdated(Instant.now());
    if (marketPlaceDAO.update(marketPlace) < 1) {
      create(marketPlace);
    }
  }

  private void create(MarketPlace marketPlace) {
    if (null == marketPlace.getId()) {
      marketPlace.setId(UUID.randomUUID());
    }

    if (null == marketPlace.getDateCreated()) {
      marketPlace.setDateCreated(Instant.now());
    }

    marketPlaceDAO.insert(marketPlace);
  }

  public MarketPlace load(String partner, String baseUrl) {
    return marketPlaceDAO.findByPartnerAndBaseUrl(partner, baseUrl);
  }
}
