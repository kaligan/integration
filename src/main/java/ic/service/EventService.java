package ic.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import ic.db.jdbi.EventDAO;
import ic.domain.Event;
import ic.resources.dto.EventDTO;
import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Author: mnicholson
 * Date: 2015/10/20
 */
public class EventService {
  Logger logger = LoggerFactory.getLogger(EventService.class);
  private final DBI dbi;
  private final Client client;

  public EventService(Client client, DBI dbi) {
    this.client = client;
    this.dbi = dbi;
  }

  public Event callJsonUri(URI uri) throws Exception {
    String eventJson = client.target(uri)
        .request(MediaType.APPLICATION_JSON)
        .get(String.class);

    try {
      ObjectMapper mapper = new ObjectMapper();
      EventDTO eventDTO = mapper.readValue(eventJson, EventDTO.class);

      Event event = new Event.Builder()
          .withEventDTO(eventDTO)
          .withRequestUrl(uri.toURL().toString())
          .withTransactionLog(eventJson)
          .create();

      this.logEvent(event);
      return event;
    } catch (IOException e) {
      logger.error(e.getMessage(), e);
      this.logEvent(
          new Event.Builder()
              .withRequestUrl(uri.toString())
              .withTransactionLog(eventJson)
              .withErrorLog(e.getMessage())
              .create()
      );
      throw new Exception(e);
    }
  }


  private Optional<Event> callXmlUri(URI uri) {
    String eventXml = client.target(uri)
        .request(MediaType.APPLICATION_XML)
        .get(String.class);

    Event event = null;
    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(EventDTO.class);
      Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
      EventDTO eventDTO = (EventDTO) unmarshaller.unmarshal(new StringReader(eventXml));
      event = new Event.Builder()
          .withEventDTO(eventDTO)
          .withRequestUrl(uri.toURL().toString())
          .withTransactionLog(eventXml)
          .create();

      this.logEvent(event);
    } catch (JAXBException | MalformedURLException e) {
      logger.error(e.getMessage(), e);
      this.logEvent(
          new Event.Builder()
              .withRequestUrl(uri.toString())
              .withTransactionLog(eventXml)
              .withErrorLog(e.getMessage())
              .create()
      );
    }

    return Optional.ofNullable(event);
  }

  /**
   * Logs the successful events for future reference by administrators.  Original request, response, and data sent
   * is logged.
   *
   * @param event - event to be logged in the database.
   */
  public void logEvent(Event event) {
    dbi.onDemand(EventDAO.class).insert(event);
  }

  /**
   * Lists all the Events in the database.
   *
   * @return a list of all events called by AppCarousel.
   */
  public List<Event> list() {
    List<Event> events = dbi.onDemand(EventDAO.class).findAll();
    return events;
  }

  /**
   * Returns a single event populated by AppCarousel.
   *
   * @param id - unique identifier assigned at save time.
   * @return Event object loaded from database identified by its unique id.
   */
  public Event load(UUID id) {
    return dbi.onDemand(EventDAO.class).findById(id);
  }
}
