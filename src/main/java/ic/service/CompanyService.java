package ic.service;

import ic.db.jdbi.CompanyDAO;
import ic.domain.Company;
import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

/**
 * Created by mnicholson on 21/10/15.
 */
public class CompanyService {
  private final Logger logger = LoggerFactory.getLogger(UserService.class);
  private final DBI dbi;
  private final CompanyDAO companyDAO;

  public CompanyService(DBI dbi) {
    this.dbi = dbi;
    this.companyDAO = dbi.onDemand(CompanyDAO.class);
  }

  public List<Company> list() {
    return companyDAO.findAll();
  }

  public Company findById(UUID id) {
    return companyDAO.findById(id);
  }


  public void save(Company company) {
    company.setLastUpdated(Instant.now());
    if (companyDAO.update(company) < 1) {
      create(company);
    }
  }

  private void create(Company company) {
    if (null == company.getId()) {
      company.setId(UUID.randomUUID());
    }

    if (null == company.getDateCreated()) {
      company.setDateCreated(Instant.now());
    }

    companyDAO.insert(company);
  }
}
