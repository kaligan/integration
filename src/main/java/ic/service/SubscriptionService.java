package ic.service;

import ic.db.jdbi.SubscriptionDAO;
import ic.domain.Subscription;
import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by mnicholson on 21/10/15.
 */
public class SubscriptionService {
  private final Logger logger = LoggerFactory.getLogger(UserService.class);
  private final DBI dbi;
  private final SubscriptionDAO subscriptionDAO;

  public SubscriptionService(DBI dbi) {
    this.dbi = dbi;
    this.subscriptionDAO = dbi.onDemand(SubscriptionDAO.class);
  }

  public List<Subscription> list() {
    return subscriptionDAO.findAll();
  }

  public Subscription findById(UUID id) {
    return subscriptionDAO.findById(id);
  }

  public Optional<Subscription> findByUserIdMarketId(UUID userId, UUID marketId) {
    return Optional.of(subscriptionDAO.findByUserIdMarketId(userId, marketId));
  }

  public void save(Subscription subscription) {
    subscription.setLastUpdated(Instant.now());
    if (subscriptionDAO.update(subscription) < 1) {
      create(subscription);
    }
  }

  private void create(Subscription subscription) {
    if (null == subscription.getId()) {
      subscription.setId(UUID.randomUUID());
    }

    if (null == subscription.getDateCreated()) {
      subscription.setDateCreated(Instant.now());
    }

    subscriptionDAO.insert(subscription);
  }
}
