package ic.service;

import ic.db.jdbi.UserDAO;
import ic.domain.User;
import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by mnicholson on 18/10/15.
 */
@Singleton
public class UserService {
  private final Logger logger = LoggerFactory.getLogger(UserService.class);
  private final DBI dbi;
  private final UserDAO userDAO;

  public UserService(DBI dbi) {
    this.dbi = dbi;
    this.userDAO = dbi.onDemand(UserDAO.class);
  }

  /**
   * Returns a list of all the users currently persisted to the database.
   *
   * @return A list of users in the database.
   */
  public List<User> list() {
    return userDAO.findAll();
  }

  /**
   * Attempts to load a user from the database identified by its unique identifier.
   *
   * @param id - User unique identifier of type UUID.
   * @return - User if it exits in the database, null otherwise.
   */
  public User findById(UUID id) {
    logger.debug("Loading user for id: " + id.toString());
    return userDAO.findById(id);
  }

  /**
   * Returns the User that has the unique appDirectId requested assigned to it.
   *
   * @param appDirectId - unique appDirect ID for user.
   * @return - Optional User if it exists in the database.
   */
  public Optional<User> findByAppDirectId(UUID appDirectId) {
    logger.debug("Loading user for appDirectId: " + appDirectId.toString());
    return Optional.ofNullable(userDAO.findByAppCarouselId(appDirectId));
  }

  /**
   * Updates the User if it exists otherwise calls the create function.
   *
   * @param user - User to be saved to the database.
   */
  public void save(User user) {
    logger.debug("Saving user: " + user.toString());
    user.setLastUpdated(Instant.now());
    if (userDAO.update(user) < 1) {
      //doesn't exist in the database, issue a create command
      this.create(user);
    }
  }

  /**
   * Assigns a new ID and DateCreated to the User if they are not already set and persists said user to the database.
   *
   * @param user - User to be inserted into the database.
   */
  public void create(User user) {
    logger.debug("Creating user: " + user.toString());

    if (null == user.getId()) {
      user.setId(UUID.randomUUID());
    }

    if (null == user.getDateCreated()) {
      user.setDateCreated(Instant.now());
    }

    user.setLastUpdated(Instant.now());
    userDAO.insert(user);
  }

  /**
   * Removes the user from the database.
   *
   * @param id - id of the user to be deleted.
   * @return - number of rows affected by the update.
   */
  public int delete(UUID id) {
    logger.debug("Deleting user with the id: " + id.toString());
    return userDAO.delete(id);
  }

  /**
   * Removes the user from the database.
   *
   * @param appDirectId - unique appDirectId of the user to be deleted.
   * @return - number of rows affected by the update.
   */
  public int deleteByAppDirectId(UUID appDirectId) {
    logger.debug("Deleting user with the appDirectId: " + appDirectId.toString());
    return userDAO.delete(appDirectId);
  }
}
