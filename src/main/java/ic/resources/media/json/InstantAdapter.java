package ic.resources.media.json;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.Instant;

/**
 * Created by mnicholson on 11/05/15.
 */
public class InstantAdapter extends XmlAdapter<String, Instant> {

  @Override
  public Instant unmarshal(String s) throws Exception {
    return Instant.parse(s);
  }

  @Override
  public String marshal(Instant d) throws Exception {
    return d.toString();
  }
}
