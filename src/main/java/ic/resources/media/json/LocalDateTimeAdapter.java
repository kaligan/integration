/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ic.resources.media.json;

import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author mnicholson
 */
public class LocalDateTimeAdapter extends XmlAdapter<String, LocalDateTime> {
  final Logger logger = LoggerFactory.getLogger(LocalDateTimeAdapter.class);
  public static final DateTimeFormatter DATE_TIME_FORMATTER_24 = DateTimeFormatter.ofPattern("yyyy-MM-dd H:m");

  @Override
  public LocalDateTime unmarshal(String s) throws Exception {
    if (null == s || Strings.isNullOrEmpty(s)) {
      return null;
    }

    return LocalDateTime.parse(s, DATE_TIME_FORMATTER_24);
  }

  @Override
  public String marshal(LocalDateTime dateTime) throws Exception {
    return dateTime.format(DATE_TIME_FORMATTER_24);
  }
}
