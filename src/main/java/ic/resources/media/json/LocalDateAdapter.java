/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ic.resources.media.json;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @author mnicholson
 */
public class LocalDateAdapter extends XmlAdapter<String, LocalDate> {
  public static final String DATE_FORMAT = "yyyy-MM-dd";
  public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);

  @Override
  public LocalDate unmarshal(String s) throws Exception {
    return LocalDate.parse(s, DATE_FORMATTER);
  }

  @Override
  public String marshal(LocalDate d) throws Exception {
    return d.format(DATE_FORMATTER);
  }
}
