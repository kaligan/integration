package ic.resources;

import ic.service.CompanyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.util.UUID;

/**
 * Created by mnicholson on 21/10/15.
 */
@Path("api/companies")
public class CompaniesResource {
  private final Logger logger = LoggerFactory.getLogger(SubscriptionsResource.class);

  @Inject
  CompanyService companyService;

  @GET
  public Response listUsers() {
    return Response.ok(companyService.list()).build();
  }

  @GET
  @Path("{id}")
  public Response load(@PathParam("id") String id) {
    UUID eventId = UUID.fromString(id);
    return Response.ok(companyService.findById(eventId)).build();
  }
}
