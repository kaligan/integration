package ic.resources.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Author: mnicholson
 * Date: 2015/10/18
 */
@XmlRootElement(name="account")
public class AccountDTO {
  private String accountIdentifier;
  private String status;

  public String getAccountIdentifier() {
    return accountIdentifier;
  }

  public void setAccountIdentifier(String accountIdentifier) {
    this.accountIdentifier = accountIdentifier;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  @Override
  public String toString() {
    return "Account{" +
        "accountIdentifier='" + accountIdentifier + '\'' +
        ", status='" + status + '\'' +
        '}';
  }
}
