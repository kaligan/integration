package ic.resources.dto;

import ic.domain.Company;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.UUID;

/**
 * Author: mnicholson
 * Date: 2015/10/21
 */

@XmlRootElement(name = "company")
public class CompanyDTO {
  private UUID uuid;
  private UUID externalId;
  private String name;
  private String email;
  private String phoneNumber;
  private String website;
  private String country;

  public UUID getUuid() {
    return uuid;
  }

  public void setUuid(UUID uuid) {
    this.uuid = uuid;
  }

  public UUID getExternalId() {
    return externalId;
  }

  public void setExternalId(UUID externalId) {
    this.externalId = externalId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getWebsite() {
    return website;
  }

  public void setWebsite(String website) {
    this.website = website;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public Company getInstance() {
    Company company = new Company();
    company.setAppCarouselId(this.uuid);
    company.setName(this.name);
    company.setEmail(this.email);
    company.setPhoneNumber(this.phoneNumber);
    company.setWebsite(this.website);
    company.setCountry(this.country);

    return company;
  }

  @Override
  public String toString() {
    return "CompanyDTO{" +
        "uuid=" + uuid +
        ", externalId=" + externalId +
        ", name='" + name + '\'' +
        ", email='" + email + '\'' +
        ", phoneNumber='" + phoneNumber + '\'' +
        ", website='" + website + '\'' +
        ", country='" + country + '\'' +
        '}';
  }
}
