package ic.resources.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Author: mnicholson
 * Date: 2015/10/16
 */
@XmlRootElement(name = "result")
public class ResultDTO {
  private Boolean success = true;
  private String accountIdentifier;
  private ErrorCode errorCode;
  private String message;

  public ResultDTO() {
  }

  public ResultDTO(Boolean success, String accountIdentifier, ErrorCode errorCode, String message) {
    this.success = success;
    this.accountIdentifier = accountIdentifier;
    this.errorCode = errorCode;
    this.message = message;
  }

  public Boolean getSuccess() {
    return success;
  }

  public void setSuccess(Boolean success) {
    this.success = success;
  }

  public String getAccountIdentifier() {
    return accountIdentifier;
  }

  public void setAccountIdentifier(String accountIdentifier) {
    this.accountIdentifier = accountIdentifier;
  }

  public ErrorCode getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(ErrorCode errorCode) {
    this.errorCode = errorCode;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }



  @Override
  public String toString() {
    return "ResultDTO{" +
        "success=" + success +
        ", accountIdentifier='" + accountIdentifier + '\'' +
        ", errorCode=" + errorCode +
        ", message='" + message + '\'' +
        '}';
  }

  public static class Builder {
    private Boolean success = true;
    private String accountIdentifier;
    private ErrorCode errorCode;
    private String message;

    public Builder withSuccess(Boolean success) {
      this.success = success;
      return this;
    }

    public Builder withAccountIdentifier(String accountIdentifier) {
      this.accountIdentifier = accountIdentifier;
      return this;
    }

    public Builder withErrorCode(ErrorCode errorCode) {
      this.errorCode = errorCode;
      this.success = false;
      return this;
    }

    public Builder withMessage(String message) {
      this.message = message;
      return this;
    }

    public ResultDTO create() {
      return new ResultDTO(success, accountIdentifier, errorCode, message);
    }
  }
}
