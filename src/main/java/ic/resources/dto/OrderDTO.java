package ic.resources.dto;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by mnicholson on 16/10/15.
 */
@XmlRootElement()
public class OrderDTO {
  private String editionCode;
  private String addonOfferingCode;
  private String pricingDuration;
  private List<ItemDTO> items;

  public String getEditionCode() {
    return editionCode;
  }

  public void setEditionCode(String editionCode) {
    this.editionCode = editionCode;
  }

  public String getAddonOfferingCode() {
    return addonOfferingCode;
  }

  public void setAddonOfferingCode(String addonOfferingCode) {
    this.addonOfferingCode = addonOfferingCode;
  }

  public String getPricingDuration() {
    return pricingDuration;
  }

  public void setPricingDuration(String pricingDuration) {
    this.pricingDuration = pricingDuration;
  }

  public List<ItemDTO> getItems() {
    return items;
  }

  public void setItem(List<ItemDTO> items) {
    this.items = items;
  }

  @Override
  public String toString() {
    return "Order{" +
        "editionCode='" + editionCode + '\'' +
        ", addonOfferingCode='" + addonOfferingCode + '\'' +
        ", pricingDuration='" + pricingDuration + '\'' +
        ", items=" + items +
        '}';
  }
}
