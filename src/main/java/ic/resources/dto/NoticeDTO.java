package ic.resources.dto;

/**
 * Author: mnicholson
 * Date: 2015/10/21
 */
public class NoticeDTO {
  private String type;
  private String message;

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  @Override
  public String toString() {
    return "Notice{" +
        "type='" + type + '\'' +
        ", message='" + message + '\'' +
        '}';
  }
}
