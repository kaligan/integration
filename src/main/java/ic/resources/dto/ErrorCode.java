package ic.resources.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by mnicholson on 16/10/15.
 */
@XmlRootElement(name = "errorCode")
public enum ErrorCode {
  USER_ALREADY_EXISTS,
  USER_NOT_FOUND,
  ACCOUNT_NOT_FOUND,
  MAX_USERS_REACHED,
  UNAUTHORIZED,
  OPERATION_CANCELLED,
  CONFIGURATION_ERROR,
  UNKNOWN_ERROR,
  PENDING
}
