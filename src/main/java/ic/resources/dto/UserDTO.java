package ic.resources.dto;

import ic.domain.User;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Map;
import java.util.UUID;

/**
 * Author: mnicholson
 * Date: 2015/10/21
 */

@XmlRootElement(name = "user")
public class UserDTO {
  private UUID uuid;
  private String openId;
  private String email;
  private String firstName;
  private String lastName;
  private String language;
  private String address;
  private Map<String, String> attributes;

  public String getOpenId() {
    return openId;
  }

  public void setOpenId(String openId) {
    this.openId = openId;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public Map<String, String> getAttributes() {
    return attributes;
  }

  public void setAttributes(Map<String, String> attributes) {
    this.attributes = attributes;
  }

  public UUID getUuid() {
    return uuid;
  }

  public void setUuid(UUID uuid) {
    this.uuid = uuid;
  }

  public User getUserInstance() {
    User user = new User();
    user.setAppCarouselId(this.uuid);
    user.setOpenId(this.openId);
    user.setEmail(this.email);
    user.setFirstName(this.firstName);
    user.setLastName(this.lastName);
    user.setLanguage(this.language);
    user.setAddress(this.address);
    user.setAttributes(attributes);

    return user;
  }
}
