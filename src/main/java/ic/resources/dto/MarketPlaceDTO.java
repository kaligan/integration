package ic.resources.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by mnicholson on 16/10/15.
 */
@XmlRootElement(name="marketplace")
public class MarketPlaceDTO {
  String partner;
  String baseUrl;

  public String getPartner() {
    return partner;
  }

  public void setPartner(String partner) {
    this.partner = partner;
  }

  public String getBaseUrl() {
    return baseUrl;
  }

  public void setBaseUrl(String baseUrl) {
    this.baseUrl = baseUrl;
  }

  @Override
  public String toString() {
    return "MarketPlace{" +
        "partner='" + partner + '\'' +
        ", baseUrl='" + baseUrl + '\'' +
        '}';
  }
}

