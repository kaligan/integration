package ic.resources.dto;

import ic.domain.Event;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Author: mnicholson
 * Date: 2015/10/21
 */
@XmlRootElement()
public class EventDTO {
  private Event.Type type;
  private MarketPlaceDTO marketplace;
  private String applicationUuid;
  private String flag;
  private UserDTO creator;
  private PayloadDTO payload;
  private String returnUrl;
  private List<String> links;

  public Event.Type getType() {
    return type;
  }

  public void setType(Event.Type type) {
    this.type = type;
  }

  public MarketPlaceDTO getMarketplace() {
    return marketplace;
  }

  public void setMarketplace(MarketPlaceDTO marketplace) {
    this.marketplace = marketplace;
  }

  public String getApplicationUuid() {
    return applicationUuid;
  }

  public void setApplicationUuid(String applicationUuid) {
    this.applicationUuid = applicationUuid;
  }

  public String getFlag() {
    return flag;
  }

  public void setFlag(String flag) {
    this.flag = flag;
  }

  public UserDTO getCreator() {
    return creator;
  }

  public void setCreator(UserDTO creator) {
    this.creator = creator;
  }

  public PayloadDTO getPayload() {
    return payload;
  }

  public void setPayload(PayloadDTO payload) {
    this.payload = payload;
  }

  public String getReturnUrl() {
    return returnUrl;
  }

  public void setReturnUrl(String returnUrl) {
    this.returnUrl = returnUrl;
  }

  public List<String> getLinks() {
    return links;
  }

  public void setLinks(List<String> links) {
    this.links = links;
  }

  @Override
  public String toString() {
    return "EventDTO{" +
        "type=" + type +
        ", marketplace=" + marketplace +
        ", applicationUuid='" + applicationUuid + '\'' +
        ", flag='" + flag + '\'' +
        ", creator=" + creator +
        ", payload=" + payload +
        ", returnUrl='" + returnUrl + '\'' +
        ", links=" + links +
        '}';
  }
}
