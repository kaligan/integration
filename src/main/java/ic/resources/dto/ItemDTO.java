package ic.resources.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by mnicholson on 16/10/15.
 */
@XmlRootElement()
public class ItemDTO {
  private Double quantity;
  private String unit;

  public Double getQuantity() {
    return quantity;
  }

  public void setQuantity(Double quantity) {
    this.quantity = quantity;
  }

  public String getUnit() {
    return unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }

  @Override
  public String toString() {
    return "Item{" +
        "quantity=" + quantity +
        ", unit='" + unit + '\'' +
        '}';
  }
}
