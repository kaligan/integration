package ic.resources.dto;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.Map;

/**
 * Author: mnicholson
 * Date: 2015/10/20
 */
@XmlRootElement(name="payload")
public class PayloadDTO {
  private UserDTO user;
  private CompanyDTO company;
  private OrderDTO order;
  private AccountDTO account;

  private String addonInstance;
  private String addonBinding;
  private NoticeDTO notice;
  private Map<String, String> configuration;

  public UserDTO getUser() {
    return user;
  }

  public void setUser(UserDTO user) {
    this.user = user;
  }

  public CompanyDTO getCompany() {
    return company;
  }

  public void setCompany(CompanyDTO company) {
    this.company = company;
  }

  public OrderDTO getOrder() {
    return order;
  }

  public void setOrder(OrderDTO order) {
    this.order = order;
  }

  public AccountDTO getAccount() {
    return account;
  }

  public void setAccount(AccountDTO account) {
    this.account = account;
  }

  public String getAddonInstance() {
    return addonInstance;
  }

  public void setAddonInstance(String addonInstance) {
    this.addonInstance = addonInstance;
  }

  public String getAddonBinding() {
    return addonBinding;
  }

  public void setAddonBinding(String addonBinding) {
    this.addonBinding = addonBinding;
  }

  public NoticeDTO getNotice() {
    return notice;
  }

  public void setNotice(NoticeDTO notice) {
    this.notice = notice;
  }

  public Map<String, String> getConfiguration() {
    return configuration;
  }

  public void setConfiguration(Map<String, String> configuration) {
    this.configuration = configuration;
  }

  @Override
  public String toString() {
    return "Payload{" +
        "user=" + user +
        ", company=" + company +
        ", order=" + order +
        ", account=" + account +
        ", addonInstance='" + addonInstance + '\'' +
        ", addonBinding='" + addonBinding + '\'' +
        ", notice='" + notice + '\'' +
        ", configuration=" + configuration +
        '}';
  }
}
