package ic.resources;

import ic.domain.*;
import ic.resources.dto.ErrorCode;
import ic.resources.dto.ResultDTO;
import ic.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.UUID;

@Path("api/ad")
public class AppDirectResource {
  private final Logger logger = LoggerFactory.getLogger(AppDirectResource.class);

  @Inject EventService eventService;
  @Inject UserService userService;
  @Inject CompanyService companyService;
  @Inject MarketService marketService;
  @Inject SubscriptionService subscriptionService;

  @GET
  @Path("subscriptions/create")
  @Produces({MediaType.APPLICATION_XML})
  public ResultDTO createNotification(@QueryParam("url") String url) {

    try {
      final URI uri = new URI(url);
      Event event = eventService.callJsonUri(uri);
      if (event.getType() != Event.Type.SUBSCRIPTION_ORDER) {
        return invalidEventTypeResultDTO();
      }

      Company company = event.getPayload().getCompany().getInstance();
      companyService.save(company);

      User creator = event.getCreator();
      userService.save(creator);

      MarketPlace marketPlace = new MarketPlace();
      marketPlace.setPartner(event.getMarketPlace().getPartner());
      marketPlace.setBaseUrl(event.getMarketPlace().getBaseUrl());
      marketService.save(marketPlace);

      Subscription subscription = new Subscription();
      subscription.setUserId(creator.getId());
      subscription.setMarketPlaceId(marketPlace.getId());
      subscription.setEditionCode(event.getPayload().getOrder().getEditionCode());
      subscription.setStatus(Subscription.Status.ACTIVE);
      subscriptionService.save(subscription);

      return new ResultDTO.Builder()
          .withAccountIdentifier(creator.getId().toString())
          .create();

    } catch (URISyntaxException | NullPointerException e) {
      return invalidUrlResultDTO();
    } catch (Exception e) {
      return exceptionResultDTO(e);
    }
  }

  @GET
  @Path("subscriptions/change")
  @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public ResultDTO changeNotification(@QueryParam("url") String url) {

    try {
      final URI uri = new URI(url);
      Event event = eventService.callJsonUri(uri);
      if (event.getType() != Event.Type.SUBSCRIPTION_CHANGE) {
        return invalidEventTypeResultDTO();
      }

      UUID userId = event.getCreator().getAppCarouselId();
      MarketPlace marketPlace = marketService.load(
          event.getMarketPlace().getPartner(), event.getMarketPlace().getBaseUrl()
      );

      subscriptionService.findByUserIdMarketId(userId, marketPlace.getId()).ifPresent(subscription -> {
        subscription.setEditionCode(event.getPayload().getOrder().getEditionCode());
        subscriptionService.save(subscription);
      });

      return new ResultDTO.Builder()
          .withAccountIdentifier(UUID.randomUUID().toString())
          .create();

    } catch (URISyntaxException | NullPointerException e) {
      return invalidUrlResultDTO();
    } catch (Exception e) {
      return exceptionResultDTO(e);
    }
  }

  @GET
  @Path("subscriptions/cancel")
  @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public ResultDTO cancelNotification(@QueryParam("url") String url) {

    try {
      final URI uri = new URI(url);
      Event event = eventService.callJsonUri(uri);
      if (event.getType() != Event.Type.SUBSCRIPTION_CANCEL) {
        return invalidEventTypeResultDTO();
      }

      UUID userId = event.getCreator().getAppCarouselId();
      MarketPlace marketPlace = marketService.load(
          event.getMarketPlace().getPartner(), event.getMarketPlace().getBaseUrl()
      );

      subscriptionService.findByUserIdMarketId(userId, marketPlace.getId()).ifPresent(subscription -> {
        subscription.setStatus(Subscription.Status.CANCELED);
        subscriptionService.save(subscription);
      });

      return new ResultDTO.Builder()
          .withAccountIdentifier(UUID.randomUUID().toString())
          .create();

    } catch (URISyntaxException | NullPointerException e) {
      return invalidUrlResultDTO();
    } catch (Exception e) {
      return exceptionResultDTO(e);
    }

  }

  @GET
  @Path("subscriptions/status")
  @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public ResultDTO statusNotification(@QueryParam("url") String url) {

    try {
      final URI uri = new URI(url);
      Event event = eventService.callJsonUri(uri);
      if (event.getType() != Event.Type.SUBSCRIPTION_NOTICE) {
        return invalidEventTypeResultDTO();
      }

      return new ResultDTO.Builder()
          .withAccountIdentifier(UUID.randomUUID().toString())
          .create();

    } catch (URISyntaxException | NullPointerException e) {
      return invalidUrlResultDTO();
    } catch (Exception e) {
      return exceptionResultDTO(e);
    }

  }

  @GET
  @Path("users/assign")
  @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public ResultDTO assignUser(@QueryParam("url") String url) {

    try {
      final URI uri = new URI(url);
      Event event = eventService.callJsonUri(uri);

      if (event.getType() != Event.Type.USER_ASSIGNMENT) {
        return invalidEventTypeResultDTO();
      }

      if (userService.findByAppDirectId(event.getPayload().getUser().getUuid()).isPresent()) {
        return (new ResultDTO.Builder()
            .withErrorCode(ErrorCode.USER_ALREADY_EXISTS)
            .withAccountIdentifier("User with that id already exists")
            .create()
        );
      }

      User user = event.getPayload().getUser().getUserInstance();
      userService.create(user);
      return new ResultDTO.Builder().create();
    } catch (URISyntaxException | NullPointerException e) {
      return invalidUrlResultDTO();
    } catch (Exception e) {
      return exceptionResultDTO(e);
    }
  }

  @GET
  @Path("users/unassigned")
  @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public ResultDTO unassignedUser(@QueryParam("url") String url) {

    try {
      final URI uri = new URI(url);
      Event event = eventService.callJsonUri(uri);

      if (event.getType() != Event.Type.USER_UNASSIGNMENT) {
        return invalidEventTypeResultDTO();
      }

      int success = userService.deleteByAppDirectId(event.getPayload().getUser().getUuid());
      if (success <= 0) {
        return new ResultDTO.Builder()
            .withErrorCode(ErrorCode.ACCOUNT_NOT_FOUND)
            .create();
      }

      return new ResultDTO.Builder().create();
    } catch (URISyntaxException | NullPointerException e) {
      return invalidUrlResultDTO();
    } catch (Exception e) {
      return exceptionResultDTO(e);
    }
  }

  /**
   * Convenience method to return the same error for invalid urls vs eventType
   *
   * @return ResultDTO with success = false and a standard message letting the client know what happened.
   */
  private ResultDTO invalidEventTypeResultDTO() {
    return new ResultDTO.Builder()
        .withErrorCode(ErrorCode.USER_NOT_FOUND)
        .withMessage("Event type doesn't match url")
        .withSuccess(false)
        .create();
  }

  private ResultDTO invalidUrlResultDTO() {
    return new ResultDTO.Builder()
        .withErrorCode(ErrorCode.CONFIGURATION_ERROR)
        .withMessage("Invalid url parameter passed")
        .withSuccess(false)
        .create();
  }

  private ResultDTO exceptionResultDTO(Exception e) {
    logger.error(e.getMessage(), e);
    return new ResultDTO.Builder()
        .withErrorCode(ErrorCode.UNKNOWN_ERROR)
        .withSuccess(false)
        .create();
  }
}
