package ic.resources;

import ic.views.*;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by mnicholson on 07/10/15.
 */

@Path("/")
@Produces(MediaType.TEXT_HTML)
public class SiteResource {

  @GET
  public IndexView getIndex() {
    return new IndexView();
  }

  @GET
  @Path("events")
  public EventListView getEventList() {
    return new EventListView();
  }

  @GET
  @Path("users")
  public UserListView getUserList() {
    return new UserListView();
  }

  @GET
  @Path("companies")
  public CompanyListView getCompanyList() {
    return new CompanyListView();
  }

  @GET
  @Path("subscriptions")
  public SubscriptionListView getSubscriptionList() {
    return new SubscriptionListView();
  }
}
