package ic.resources;

import ic.service.EventService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.util.UUID;

/**
 * Author: mnicholson
 * Date: 2015/10/21
 */
@Path("api/events")
public class EventsResource {
  private final Logger logger = LoggerFactory.getLogger(EventsResource.class);

  @Inject
  EventService eventService;

  @GET
  public Response listEvents() {
    return Response.ok(eventService.list()).build();
  }

  @GET
  @Path("{id}")
  public Response load(@PathParam("id") String id) {
    UUID eventId = UUID.fromString(id);
    return Response.ok(eventService.load(eventId)).build();
  }
}
