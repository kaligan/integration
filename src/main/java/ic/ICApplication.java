package ic;

import ic.resources.*;
import ic.service.*;
import ic.views.mustache.MustacheNoCacheViewRenderer;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.flyway.FlywayBundle;
import io.dropwizard.flyway.FlywayFactory;
import io.dropwizard.java8.Java8Bundle;
import io.dropwizard.java8.jdbi.DBIFactory;
import io.dropwizard.java8.jdbi.args.InstantArgumentFactory;
import io.dropwizard.java8.jdbi.args.InstantMapper;
import io.dropwizard.java8.jdbi.args.LocalDateArgumentFactory;
import io.dropwizard.java8.jdbi.args.LocalDateMapper;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;
import io.dropwizard.views.ViewMessageBodyWriter;
import io.dropwizard.views.ViewRenderer;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.filter.LoggingFilter;
import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import java.util.Collections;
import java.util.Map;

import static com.google.common.base.MoreObjects.firstNonNull;

/**
 * Created by: mnicholson
 * Date: 29/09/15.
 */
public class ICApplication extends Application<ICConfiguration> {
  private static Logger logger = LoggerFactory.getLogger(ICApplication.class);

  public static void main(String[] args) throws Exception {
    new ICApplication().run(args);
  }

  @Override
  public String getName() {
    return "Emergency Response Application";
  }

  @Override
  public void initialize(Bootstrap<ICConfiguration> bootstrap) {
    bootstrap.addBundle(new Java8Bundle());

    //serves static files in resources/assets
    bootstrap.addBundle(new AssetsBundle());

    //mustache views
    bootstrap.addBundle(new ViewBundle<ICConfiguration>() {
      @Override
      public void run(ICConfiguration configuration, Environment environment) throws Exception {
        if (configuration.getCacheViews()) {
          super.run(configuration, environment);
        } else {
          Map<String, Map<String, String>> options = getViewConfiguration(configuration);
          final ViewRenderer viewRenderer = new MustacheNoCacheViewRenderer();

          Map<String, String> viewOptions = options.get(viewRenderer.getSuffix());
          viewRenderer.configure(firstNonNull(viewOptions, Collections.<String, String>emptyMap()));
          environment.jersey().register(new ViewMessageBodyWriter(environment.metrics(), Collections.singletonList(viewRenderer)));
        }
        super.run(configuration, environment);
      }
    });

    //flyway database migrations
    bootstrap.addBundle(new FlywayBundle<ICConfiguration>() {
      @Override
      public DataSourceFactory getDataSourceFactory(ICConfiguration configuration) {
        return configuration.getHikariDataSourceFactory();
      }

      @Override
      public FlywayFactory getFlywayFactory(ICConfiguration configuration) {
        return configuration.getFlywayFactory();
      }
    });
  }

  @Override
  public void run(ICConfiguration configuration, Environment environment) throws Exception {
    final DBIFactory factory = new DBIFactory();
    final DBI jdbi = factory.build(environment, configuration.getHikariDataSourceFactory(), "h2");
    jdbi.registerArgumentFactory(new LocalDateArgumentFactory());
    jdbi.registerArgumentFactory(new InstantArgumentFactory());
    jdbi.registerMapper(new LocalDateMapper());
    jdbi.registerMapper(new InstantMapper());

    //Jersey client
    final Client client = new JerseyClientBuilder(environment).using(configuration.getJerseyClientConfiguration())
        .build(getName());
    client.register(new LoggingFilter(java.util.logging.Logger.getLogger(ICApplication.class.getName()), true));

    environment.jersey().register(new AbstractBinder() {
      @Override
      protected void configure() {
        bind(new CompanyService(jdbi)).to(CompanyService.class);
        bind(new EventService(client, jdbi)).to(EventService.class);
        bind(new MarketService(jdbi)).to(MarketService.class);
        bind(new SubscriptionService(jdbi)).to(SubscriptionService.class);
        bind(new UserService(jdbi)).to(UserService.class);
      }
    });

    //resources
    environment.jersey().register(new AppDirectResource());
    environment.jersey().register(new CompaniesResource());
    environment.jersey().register(new EventsResource());
    environment.jersey().register(new SiteResource());
    environment.jersey().register(new SubscriptionsResource());
    environment.jersey().register(new UsersResource());

    //cleanup on shutdown
    Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
      @Override
      public void run() {
        client.close();
      }
    }));
  }
}
