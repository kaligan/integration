package ic.db;

import com.codahale.metrics.MetricRegistry;
import com.zaxxer.hikari.HikariConfig;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.db.ManagedDataSource;

/**
 * Created by: mnicholson
 * Date: 29/09/15.
 */
public class HikariDataSourceFactory extends DataSourceFactory {

  @Override
  public ManagedDataSource build(MetricRegistry metricRegistry, String name) {
    final HikariConfig config = new HikariConfig();

    config.setDriverClassName(getDriverClass());
    config.setUsername(getUser());
    config.setPassword(getPassword());
    config.setJdbcUrl(getUrl());
    config.setConnectionTestQuery(getValidationQuery());
    config.setMaximumPoolSize(getMaxSize());

    if(getValidationQueryTimeout().isPresent()){
      config.setValidationTimeout(getValidationQueryTimeout().get().toMilliseconds());
    }

    //TODO overload the method to pass the environment so the health check can be registered as well
    config.setMetricRegistry(metricRegistry);
    return new HikariManagedDataSource(config);
  }
}
