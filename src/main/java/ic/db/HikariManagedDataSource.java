package ic.db;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

/**
 * Created by: mnicholson
 * Date: 29/09/15.
 */
public class HikariManagedDataSource extends HikariDataSource implements io.dropwizard.db.ManagedDataSource {

  public HikariManagedDataSource(final HikariConfig config) {
    super(config);
  }

  @Override
  public void start() throws Exception {
    // no op
  }

  @Override
  public void stop() throws Exception {
    close();
  }
}
