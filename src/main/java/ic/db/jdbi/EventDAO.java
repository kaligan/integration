package ic.db.jdbi;

import ic.db.jdbi.mapper.EventMapper;
import ic.domain.Event;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;
import java.util.UUID;

/**
 * Author: mnicholson
 * Date: 2015/10/20
 */
@RegisterMapper(EventMapper.class)
public interface EventDAO {
  @SqlQuery("SELECT * FROM event")
  List<Event> findAll();

  @SqlQuery("SELECT * FROM event WHERE id=:id")
  Event findById(@Bind("id") UUID uuid);

  @SqlUpdate("INSERT INTO " +
      "event(id, date_created, last_updated, type, request_url, return_url, flag, transaction_log, error_log, creator_id) " +
      "VALUES( :id, :dateCreated, :lastUpdated, :type, :requestUrl, :returnUrl, :flag, :transactionLog, :errorLog, :creatorId)")
  int insert(@BindBean Event event);
}
