package ic.db.jdbi;

import ic.db.jdbi.mapper.SubscriptionMapper;
import ic.domain.Subscription;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;
import java.util.UUID;

/**
 * Created by mnicholson on 21/10/15.
 */
@RegisterMapper(SubscriptionMapper.class)
public interface SubscriptionDAO {
  @SqlQuery("SELECT * FROM subscription")
  List<Subscription> findAll();

  @SqlQuery("SELECT * FROM subscription WHERE id=:id limit 1")
  Subscription findById(@Bind("id") UUID id);

  @SqlQuery("SELECT * FROM subscription WHERE user_id=:userId and market_place_id=:marketPlaceId limit 1")
  Subscription findByUserIdMarketId(UUID userId, UUID marketPlaceId);

  @SqlUpdate("INSERT INTO subscription(id, date_created, last_updated, edition_code, status, market_place_id, user_id) " +
      "VALUES( :id, :dateCreated, :lastUpdated, :editionCode, :status, :marketPlaceId, :userId)")
  int insert(@BindBean Subscription subscription);

  @SqlUpdate("UPDATE subscription SET last_updated=:lastUpdated, edition_code=:editionCode, status=:status, market_place_id=:marketPlaceId, user_id=:userId WHERE id=:id")
  int update(@BindBean Subscription subscription);

}
