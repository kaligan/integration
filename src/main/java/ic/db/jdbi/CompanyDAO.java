package ic.db.jdbi;

import ic.db.jdbi.mapper.CompanyMapper;
import ic.domain.Company;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;
import java.util.UUID;

/**
 * Created by mnicholson on 21/10/15.
 */
@RegisterMapper(CompanyMapper.class)
public interface CompanyDAO {
  @SqlQuery("SELECT * FROM company")
  List<Company> findAll();

  @SqlQuery("SELECT * FROM company WHERE id=:id limit 1")
  Company findById(@Bind("id") UUID uuid);

  @SqlQuery("SELECT * FROM company WHERE app_carousel_id=:id limit 1")
  Company findByAppCarouselId(@Bind("id") UUID uuid);

  @SqlUpdate("INSERT INTO " +
      "company(id, app_carousel_id, date_created, last_updated, name, email, phone_number, website, country) " +
      "VALUES( :id, :appCarouselId, :dateCreated, :lastUpdated, :name, :email, :phoneNumber, :website, :country)")
  int insert(@BindBean Company company);

  @SqlUpdate("UPDATE company SET last_updated=:lastUpdated, app_carousel_id=:appCarouselId, name=:name, email=:email, phone_number=:phoneNumber, website=:website, country=:country WHERE id=:id")
  int update(@BindBean Company company);

  @SqlUpdate("DELETE FROM company WHERE id=:id")
  int delete(@Bind("id") UUID id);

  @SqlUpdate("DELETE FROM company WHERE id=:id")
  int deleteByAppDirectId(@Bind("id") UUID id);

  void close();
}
