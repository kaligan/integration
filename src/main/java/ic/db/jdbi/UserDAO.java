package ic.db.jdbi;

import ic.db.jdbi.mapper.UserMapper;
import ic.domain.User;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;
import java.util.UUID;

/**
 * Author: mnicholson
 * Date: 2015/10/19
 */
@RegisterMapper(UserMapper.class)
public interface UserDAO {
  @SqlQuery("SELECT * FROM user")
  List<User> findAll();

  @SqlQuery("SELECT * FROM user WHERE id=:id limit 1")
  User findById(@Bind("id") UUID uuid);

  @SqlQuery("SELECT * FROM user WHERE app_carousel_id=:id limit 1")
  User findByAppCarouselId(@Bind("id") UUID uuid);

  @SqlQuery("SELECT * FROM user WHERE email=:email")
  User findByEmail(@Bind("email") String email);

  @SqlUpdate("INSERT INTO user(id, app_carousel_id, date_created, last_updated, open_id, email, first_name, last_name, language, company_department, company_title, hourly_rate, zip_code, time_zone, favorite_color, app_admin) " +
      "VALUES( :id, :appCarouselId, :dateCreated, :lastUpdated, :openId, :email, :firstName, :lastName, :language, :companyDepartment,  :companyTitle, :hourlyRate, :zipCode, :timeZone, :favoriteColor, :appAdmin)")
  int insert(@BindBean User u);

  @SqlUpdate(
      "UPDATE user " +
          "SET last_updated=:lastUpdated, app_carousel_id=:appCarouselId, open_id=:openId, email=:email, " +
          "first_name=:firstName, last_name=:lastName, language=:language, company_department=:companyDepartment, " +
          "company_title=:companyTitle, hourly_rate=:hourlyRate, zip_code=:zipCode, time_zone=:timeZone, " +
          "favorite_color=:favoriteColor, app_admin=:appAdmin WHERE id=:id")
  int update(@BindBean User u);

  @SqlUpdate("DELETE FROM user WHERE id=:id")
  int delete(@Bind("id") UUID id);

  @SqlUpdate("DELETE FROM user WHERE app_carousel_id=:id")
  int deleteByAppDirectId(@Bind("id") UUID id);

  void close();
}
