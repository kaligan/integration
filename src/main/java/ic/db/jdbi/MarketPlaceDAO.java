package ic.db.jdbi;

import ic.db.jdbi.mapper.MarketPlaceMapper;
import ic.domain.MarketPlace;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;
import java.util.UUID;

/**
 * Created by mnicholson on 21/10/15.
 */
@RegisterMapper(MarketPlaceMapper.class)
public interface MarketPlaceDAO {
  @SqlQuery("SELECT * FROM market_place")
  List<MarketPlace> findAll();

  @SqlQuery("SELECT * FROM market_place WHERE id=:id limit 1")
  MarketPlace findById(@Bind("id") UUID id);

  @SqlQuery("SELECT * FROM market_place WHERE partner=:partner and base_url=:baseUrl limit 1")
  MarketPlace findByPartnerAndBaseUrl(String partner, String baseUrl);

  @SqlUpdate("INSERT INTO market_place(id, date_created, last_updated, partner, base_url) VALUES( :id, :dateCreated, :lastUpdated, :partner, :baseUrl)")
  int insert(@BindBean MarketPlace marketPlace);

  @SqlUpdate("UPDATE market_place SET last_updated=:lastUpdated, partner=:partner, base_url=:baseUrl WHERE id=:id")
  int update(@BindBean MarketPlace marketPlace);
}
