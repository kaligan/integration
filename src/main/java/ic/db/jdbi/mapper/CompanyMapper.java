package ic.db.jdbi.mapper;

import ic.domain.Company;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by mnicholson on 21/10/15.
 */
public class CompanyMapper extends MappingUtils implements ResultSetMapper<Company> {
  @Override
  public Company map(int index, ResultSet r, StatementContext ctx) throws SQLException {
    Company c = new Company();
    c.setId(getId(r));
    c.setAppCarouselId(getUUID(r, "app_carousel_id"));
    c.setDateCreated(getInstant(r, "date_created"));
    c.setLastUpdated(getInstant(r, "last_updated"));
    c.setName(r.getString("name"));
    c.setEmail(r.getString("email"));
    c.setPhoneNumber(r.getString("phone_number"));
    c.setWebsite(r.getString("website"));
    c.setCountry(r.getString("country"));

    return c;
  }
}
