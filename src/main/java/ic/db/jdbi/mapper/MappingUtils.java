package ic.db.jdbi.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;

/**
 * Author: mnicholson
 * Date: 2015/10/20
 */
public abstract class MappingUtils {

  protected UUID getId(ResultSet r) throws SQLException {
    return getUUID(r, "id");
  }

  protected UUID getUUID(ResultSet r, String column) throws SQLException {
    return (UUID) r.getObject(column);
  }

  protected Instant getInstant(ResultSet r, String column) throws SQLException {
    Timestamp dc = r.getTimestamp("date_created");
    if (null != dc) {
      return dc.toInstant();
    } else {
      return null;
    }
  }
}
