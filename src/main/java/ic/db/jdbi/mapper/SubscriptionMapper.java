package ic.db.jdbi.mapper;

import ic.domain.Subscription;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by mnicholson on 21/10/15.
 */
public class SubscriptionMapper extends MappingUtils implements ResultSetMapper<Subscription> {
  @Override
  public Subscription map(int index, ResultSet r, StatementContext ctx) throws SQLException {
    Subscription s = new Subscription();
    s.setId(getId(r));
    s.setDateCreated(getInstant(r, "date_created"));
    s.setLastUpdated(getInstant(r, "last_updated"));
    s.setEditionCode(r.getString("edition_code"));
    s.setStatus(Subscription.Status.valueOf(r.getString("status")));
    s.setMarketPlaceId(getUUID(r, "market_place_id"));
    s.setUserId(getUUID(r, "user_id"));

    return s;
  }
}
