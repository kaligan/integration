package ic.db.jdbi.mapper;

import ic.domain.Event;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

/**
 * Author: mnicholson
 * Date: 2015/10/20
 */
public class EventMapper extends MappingUtils implements ResultSetMapper<Event> {

  @Override
  public Event map(int index, ResultSet r, StatementContext ctx) throws SQLException {
    return new Event.Builder()
        .withId((UUID) r.getObject("id"))
        .withDateCreated(getInstant(r, "date_created"))
        .withLastUpdated(getInstant(r, "last_updated"))
        .withType(Event.Type.valueOf(r.getString("type")))
        .withRequestUrl(r.getString("request_url"))
        .withReturnUrl(r.getString("return_url"))
        .withFlag(r.getString("flag"))
        .withTransactionLog(r.getString("transaction_log"))
        .withErrorLog(r.getString("error_log"))
        .withCreatorId((UUID) r.getObject("creator_id"))
        .create();
  }
}
