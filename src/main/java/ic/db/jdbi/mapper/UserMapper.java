package ic.db.jdbi.mapper;

import ic.domain.User;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

/**
 * Author: mnicholson
 * Date: 2015/10/19
 */
public class UserMapper extends MappingUtils implements ResultSetMapper<User> {
  @Override
  public User map(int index, ResultSet r, StatementContext ctx) throws SQLException {
    User user = new User();
    user.setId((UUID) r.getObject("id"));
    user.setDateCreated(getInstant(r, "date_created"));
    user.setLastUpdated(getInstant(r, "last_updated"));

    user.setOpenId(r.getString("open_id"));
    user.setEmail(r.getString("email"));
    user.setFirstName(r.getString("first_name"));
    user.setLastName(r.getString("last_name"));
    user.setLanguage(r.getString("language"));
    user.setCompanyDepartment(r.getString("company_department"));
    user.setHourlyRate(r.getString("hourly_rate"));
    user.setZipCode(r.getString("zip_code"));
    user.setTimeZone(r.getString("time_zone"));
    user.setFavoriteColor(r.getString("favorite_color"));
    user.setAppAdmin(r.getBoolean("app_admin"));
    return user;
  }
}
