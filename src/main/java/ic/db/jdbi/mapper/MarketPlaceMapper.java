package ic.db.jdbi.mapper;

import ic.domain.MarketPlace;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by mnicholson on 21/10/15.
 */
public class MarketPlaceMapper extends MappingUtils implements ResultSetMapper<MarketPlace> {
  @Override
  public MarketPlace map(int index, ResultSet r, StatementContext ctx) throws SQLException {
    MarketPlace m = new MarketPlace();
    m.setId(getId(r));
    m.setDateCreated(getInstant(r, "date_created"));
    m.setLastUpdated(getInstant(r, "last_updated"));
    m.setPartner(r.getString("partner"));
    m.setBaseUrl(r.getString("base_url"));

    return m;
  }
}
