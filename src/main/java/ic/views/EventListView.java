package ic.views;

import io.dropwizard.views.View;

/**
 * Author: mnicholson
 * Date: 2015/10/21
 */
public class EventListView extends View {

  public EventListView() {
    super("event_list.mustache");
  }
}
