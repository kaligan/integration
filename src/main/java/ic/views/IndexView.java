package ic.views;

import io.dropwizard.views.View;

/**
 * Created by mnicholson on 07/10/15.
 */
public class IndexView extends View {

  public IndexView() {
    super("event_list.mustache");
  }
}
