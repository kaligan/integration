package ic.views;

import io.dropwizard.views.View;

/**
 * Author: mnicholson
 * Date: 2015/10/21
 */
public class CompanyListView extends View {

  public CompanyListView() {
    super("company_list.mustache");
  }
}
