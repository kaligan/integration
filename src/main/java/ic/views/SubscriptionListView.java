package ic.views;

import io.dropwizard.views.View;

/**
 * Author: mnicholson
 * Date: 2015/10/21
 */
public class SubscriptionListView extends View {

  public SubscriptionListView() {
    super("subscription_list.mustache");
  }
}
