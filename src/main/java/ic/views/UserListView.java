package ic.views;

import io.dropwizard.views.View;

/**
 * Created by mnicholson on 21/10/15.
 */
public class UserListView extends View {

  public UserListView() {
    super("user_list.mustache");
  }
}
